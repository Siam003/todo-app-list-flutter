import 'package:flutter/material.dart';
import 'package:todo_app_list_flutter/pages/dialogue.dart';

import 'list_tiles.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  List toDoList = [
    ['Prayer', false],
    ['Study', false],
    ['Office Work', false],
    ['Workout', false],
    ['Refreshment', false]
  ];

  final _controller = TextEditingController();

  void checkboxTapped(bool? value, int index) {
    setState(() {
      toDoList[index][1] = !toDoList[index][1];
    });
  }

  void onSaveAction() {
    print('here');
    setState(() {
      toDoList.add([_controller.text, false]);
      _controller.clear();
    });
    Navigator.of(context).pop();
  }

  void createNewTask() {
    showDialog(
        context: context,
        builder: (context) {
          return Dialogue(
            textController: _controller,
            onSave: onSaveAction,
            onCancel: () => Navigator.of(context).pop(),
          );
        });
  }

  void deleteAction(int index) {
    setState(() {
      toDoList.removeAt(index);
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[300],
      appBar: AppBar(
        title: Text('Todo List'),
        centerTitle: true,
        backgroundColor: Colors.lime[500],
        elevation: 5,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => createNewTask(),
        child: Icon(Icons.add),
      ),
      body: ListView.builder(
          itemCount: toDoList.length,
          itemBuilder: (context, index) {
            return ListTiles(
                taskName: toDoList[index][0],
                taskComplete: toDoList[index][1],
                onChanged: (functionName) =>
                    checkboxTapped(functionName, index),
                deleteFunction: (context) => deleteAction(index));
          }),
    );
  }
}
