import 'package:flutter/material.dart';
import 'package:todo_app_list_flutter/pages/my_buttons.dart';

class Dialogue extends StatelessWidget {
  final textController;
  VoidCallback onSave;
  VoidCallback onCancel;

  Dialogue(
      {super.key,
      required this.textController,
      required this.onSave,
      required this.onCancel});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.white,
      content: Container(
        height: 200,
        width: 200,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextField(
              controller: textController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Add a new task',
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                MyButton(
                    buttonLabel: 'Save',
                    MyColor: Colors.green,
                    onPressed: onSave),
                SizedBox(
                  width: 10,
                ),
                MyButton(
                    buttonLabel: 'Cancel',
                    MyColor: Colors.red,
                    onPressed: onCancel)
              ],
            )
          ],
        ),
      ),
    );
  }
}
