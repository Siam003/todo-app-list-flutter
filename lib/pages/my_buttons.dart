import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final String buttonLabel;
  final Color MyColor;
  VoidCallback onPressed;

  MyButton(
      {super.key,
      required this.buttonLabel,
      required this.MyColor,
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      child: Text(buttonLabel),
      color: MyColor,
      onPressed: this.onPressed,
    );
  }
}
